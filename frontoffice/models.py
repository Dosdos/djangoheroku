from django.contrib.auth.models import User
from django.db import models

from utilities.validators import is_true


class UserProfile(models.Model):
    user = models.OneToOneField(User, verbose_name='User', related_name='user_profile', on_delete=models.CASCADE)
    address = models.CharField(verbose_name='Address', max_length=255, null=True, blank=True)
    image_url = models.URLField(verbose_name='Image URL', null=True, blank=True)
    is_email_verified = models.BooleanField(verbose_name='Is email verified', default=False, db_index=True)
    has_accepted_terms_and_conditions = models.BooleanField(validators=[is_true], default=False, db_index=True)

    class Meta:
        verbose_name = 'User Profile'
        verbose_name_plural = 'User Profiles'

    @property
    def username(self):
        return self.user.username

    @property
    def first_name(self):
        return self.user.first_name

    @property
    def last_name(self):
        return self.user.last_name

    def __str__(self):
        return self.user.username
