from django.conf import settings
from django.conf.urls.i18n import i18n_patterns
from django.conf.urls.static import static
from django.contrib import admin
from django.contrib.auth import views
from django.urls import path, include
from django.views.generic import TemplateView

from djangoheroku.views import change_lang_en, change_lang_it, login_user, signup_user
from frontoffice.forms import PasswordChangeForm

urlpatterns = [
    path('admin/', admin.site.urls),
    path('backoffice/', include('backoffice.urls')),
    path('robots.txt', TemplateView.as_view(template_name='robots.txt', content_type='text/plain')),
]

urlpatterns += i18n_patterns(

    # authentication
    path('login', login_user, name='login'),
    path('signup', signup_user, name='signup'),
    path('logout', views.LogoutView.as_view(template_name='auth/logout.html'), name='logout'),
    path('password/change/', views.PasswordChangeView.as_view(
        template_name='auth/password_change.html',
        form_class=PasswordChangeForm,
    ), name='password_change'),
    path('password/change/done/', views.PasswordChangeDoneView.as_view(
        template_name='auth/password_change_done.html',
    ), name='password_change_done'),
    # path('password/reset/', views.PasswordResetView.as_view(
    #     form_class=PasswordResetForm,
    #     template_name='auth/password_reset_form.html',
    #     email_template_name='email/auth_password_reset_email.html',
    #     html_email_template_name='email/auth_password_reset_email_html.html',
    #     subject_template_name='email/registration/password_reset_subject.txt',
    # ), name='password_reset'),
    # path('password/reset/done/', views.PasswordResetDoneView.as_view(
    #     template_name='auth/password_reset_done.html',
    # ), name='password_reset_done'),
    # path('password/reset/<uidb64>/<token>/', views.PasswordResetConfirmView.as_view(
    #     form_class=NewPasswordForm,
    #     template_name='auth/password_reset_confirm.html',
    # ), name='password_reset_confirm'),
    # path('password/reset/complete/', views.PasswordResetCompleteView.as_view(
    #     template_name='auth/password_reset_complete.html',
    # ), name='password_reset_complete'),

    # frontoffice
    path('', include('frontoffice.urls')),

    # Change language: en, it
    path('en', change_lang_en, name='change_to_en'),
    path('it', change_lang_it, name='change_to_it'),

)

if settings.DEBUG:
    import debug_toolbar
    urlpatterns += static(settings.STATIC_URL, document_root=settings.STATIC_ROOT)
    urlpatterns += [
        path('__debug__/', include(debug_toolbar.urls)),
    ]

admin.site.site_header = 'DjangoHeroku Admin Panel'
admin.site.site_title = 'DjangoHeroku Admin Panel'
