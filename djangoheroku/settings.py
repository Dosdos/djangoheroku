import os

from django.utils.translation import gettext_lazy as _

# Build paths inside the project like this: os.path.join(BASE_DIR, ...)
BASE_DIR = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))

SECRET_KEY = 'zxu_l$p+swpk7sg&(3&*@j0jai&+ov+@okh59x+r5v6tfhquin'  # for local usage only

DEBUG = True

ALLOWED_HOSTS = ('localhost', '127.0.0.1')


# Application definition
INSTALLED_APPS = [

    # local apps
    'backoffice',
    'frontoffice',

    # third party apps here

    # Django apps
    'django.contrib.admin',
    'django.contrib.auth',
    'django.contrib.contenttypes',
    'django.contrib.sessions',
    'django.contrib.messages',
    'django.contrib.staticfiles',
]

MIDDLEWARE = [
    'django.middleware.security.SecurityMiddleware',
    'django.contrib.sessions.middleware.SessionMiddleware',
    'django.middleware.locale.LocaleMiddleware',
    'django.middleware.common.CommonMiddleware',
    'django.middleware.csrf.CsrfViewMiddleware',
    'django.contrib.auth.middleware.AuthenticationMiddleware',
    'django.contrib.messages.middleware.MessageMiddleware',
    'django.middleware.clickjacking.XFrameOptionsMiddleware',
]

ROOT_URLCONF = 'djangoheroku.urls'

TEMPLATES = [
    {
        'BACKEND': 'django.template.backends.django.DjangoTemplates',
        'DIRS': [os.path.join(BASE_DIR, 'templates')],
        'APP_DIRS': True,
        'OPTIONS': {
            'context_processors': [
                'django.template.context_processors.debug',
                'django.template.context_processors.request',
                'django.contrib.auth.context_processors.auth',
                'django.contrib.messages.context_processors.messages',
                'django.template.context_processors.static',
                # 'api.context_processors.api_constants',
            ],
        },
    },
]

WSGI_APPLICATION = 'djangoheroku.wsgi.application'

# Database
DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.sqlite3',
        'NAME': os.path.join(BASE_DIR, 'djangoheroku/db.sqlite3'),
    }
}

# Default primary key field type
DEFAULT_AUTO_FIELD = 'django.db.models.BigAutoField'

# Password validation
AUTH_PASSWORD_VALIDATORS = []

# Internationalization and localization
LANGUAGE_CODE = 'en'
TIME_ZONE = 'Europe/Rome'  # 'UTC'
USE_I18N = True
USE_L10N = True
USE_TZ = True
LOCALE_PATHS = (BASE_DIR + '/locale', )
LANGUAGES = (
    ('en', _('English')),
    ('it', _('Italian')),
)

# Static files (CSS, JavaScript, Images)
STATIC_ROOT = os.path.join(BASE_DIR, 'staticfiles')
STATIC_URL = '/static/'

# Extra places for collectstatic to find static files.
STATICFILES_DIRS = (
    os.path.join(BASE_DIR, 'djangoheroku/static'),
)

# List of finder classes that know how to find static files in various locations.
STATICFILES_FINDERS = (
    'django.contrib.staticfiles.finders.FileSystemFinder',
    'django.contrib.staticfiles.finders.AppDirectoriesFinder',
)

# Url configurations
DOMAIN = 'your-nice-domain.com'
DOMAIN_PROTOCOL = 'http'
DOMAIN_URL = DOMAIN_PROTOCOL + '://127.0.0.1:8000'
LOGIN_URL = 'rest_framework:login'
LOGOUT_URL = 'rest_framework:logout'
PRIVACY_POLICY_URL = ""
FACEBOOK_APP_ID = "..."
EMAIL_ADDRESS_FOR_SUPPORT = "support@" + DOMAIN

# Email configurations
EMAIL_BACKEND = 'django.core.mail.backends.console.EmailBackend'  # redirect email to stdout for development
EMAIL_PORT = 587
EMAIL_USE_TLS = True
DEFAULT_FROM_EMAIL = f'djangoheroku <no-reply@{DOMAIN}>'
EMAIL_ADDRESS_INFO = f'info@{DOMAIN}'

# Loading test/prod settings based on ENV settings
ENV = os.environ.get('ENV')

if ENV == 'prod':
    try:
        from .production_settings import *
        MIDDLEWARE.append('whitenoise.middleware.WhiteNoiseMiddleware',)
    except ImportError:
        pass

if DEBUG:
    INTERNAL_IPS = ('127.0.0.1', )
    INSTALLED_APPS.append('debug_toolbar')
    MIDDLEWARE.append('debug_toolbar.middleware.DebugToolbarMiddleware',)
