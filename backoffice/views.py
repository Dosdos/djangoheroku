from django.contrib.auth.decorators import login_required, user_passes_test
from django.shortcuts import render


# ===========================================================================
# USER PASSES TESTS
# ===========================================================================
def is_admin(user):
    return user.is_staff


# ===========================================================================
# HOME
# ===========================================================================
@login_required(login_url='/login/')
@user_passes_test(is_admin)
def home(request, template_name="backoffice/home.html"):
    return render(request, template_name, {
        'session': request.session.keys(),
    })
